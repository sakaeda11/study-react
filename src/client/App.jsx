import React, {PropTypes} from "react";
import {connect} from "react-redux";
import Hello from "./Hello";

const App = ({todos, addTodo}) => {
    const onSubmit = (event) => {
        event.preventDefault();
        const ele = event.target.getElementsByTagName('input')[0];
        addTodo({text: ele.value});
        ele.value = '';
    };
    return (
        <div>
            <h1>TODOs testing HMR</h1>
            <Hello />
            <ul>
                {todos.map(({text}) => <li>{text}
                    <button onClick={onDelete}>x</button>
                </li>)}
                <li>
                    <form onSubmit={onSubmit}>
                        <input type="text"/>
                    </form>
                </li>
            </ul>
        </div>
    );
};

App.propTypes = {
    todos: PropTypes.arrayOf(PropTypes.shape({
        text: PropTypes.string,
    })).isRequired,
    addTodo: PropTypes.func.isRequired,
};

const mapStateToProps = ({todos}) => ({todos});
const mapDispatchToProps = dispatch => ({
    addTodo: todo => dispatch({type: 'ADD_TODO', todo}),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

