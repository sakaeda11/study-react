export default (state = {todos: []}, action) => {
    if (action.type === 'ADD_TODO') {
        return {
            ...state,
            todos: [...state.todos, {text: action.todo.text + '!!!'}],
        };
    }
    if (action.type === 'DELETE_TODO') {
        const index = action.index;
        const todos = state.todos;
        return {
            ...state,
            todos: [].concat(todos.slice(0, index), todos.slice(index + 1)),
        };
    }
    return state;
};
